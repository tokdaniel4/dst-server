-- Use this file to enable and configure your mods. The mod will only be available in the game
-- if you set "enabled=true"!!!
--
-- Also, during the container startup this file will be copied to both Master/ and Caves/ folders. What's setup here
-- will be available in both shards!
--
-- See the example below:

return {
  ["workshop-347079953"]={
    configuration_options={ DFV_Language="EN", DFV_MinimalMode="default" },
    enabled=true
  },
  ["workshop-356930882"]={
    configuration_options={ uses=10000000, uses2=10000000, uses3=10000000 },
    enabled=true
  },
  ["workshop-362175979"]={ configuration_options={ ["Draw over FoW"]="disabled" }, enabled=true },
  ["workshop-374550642"]={ configuration_options={ MAXSTACKSIZE=150 }, enabled=true },
  ["workshop-375438821"]={
    configuration_options={ EVERGREEN_CHOPS_NORMAL=10, EVERGREEN_CHOPS_SMALL=5, EVERGREEN_CHOPS_TALL=15 },
    enabled=true
  },
  ["workshop-375859599"]={
    configuration_options={
      divider=5,
      random_health_value=0,
      random_range=0,
      send_unknwon_prefabs=false,
      show_type=0,
      unknwon_prefabs=1,
      use_blacklist=true
    },
    enabled=true
  },
  ["workshop-378160973"]={
    configuration_options={
      ENABLEPINGS=true,
      FIREOPTIONS=2,
      OVERRIDEMODE=false,
      SHAREMINIMAPPROGRESS=true,
      SHOWFIREICONS=true,
      SHOWPLAYERICONS=true,
      SHOWPLAYERSOPTIONS=2
    },
    enabled=true
  },
  ["workshop-471111069"]={ configuration_options={  }, enabled=true },
  ["workshop-501385076"]={
    configuration_options={
      quick_cook_on_fire=true,
      quick_harvest=true,
      quick_pick_cactus=true,
      quick_pick_plant_normal_ground=true
    },
    enabled=true
  },
  ["workshop-816057392"]={
    configuration_options={
      FLIPMOON=false,
      HIDECAVECLOCK=false,
      HUDSCALEFACTOR=100,
      SEASONOPTIONS="Clock",
      SHOWBEAVERNESS=true,
      SHOWCLOCKTEXT=true,
      SHOWMAXONNUMBERS=true,
      SHOWMOON=1,
      SHOWNAUGHTINESS=true,
      SHOWNEXTFULLMOON=true,
      SHOWSTATNUMBERS=true,
      SHOWTEMPBADGES=true,
      SHOWTEMPERATURE=true,
      SHOWWANINGMOON=true,
      SHOWWORLDTEMP=false,
      UNIT="T"
    },
    enabled=true
  }
}